package testmaven;
%%
%class Aléxico
%public
%standalone
%unicode
ENTERO  = [1-9][0-9]* 
COMI = "/*"
COMD =  "*/"
COMEN = "//"
COMENTARIO = {COMI}{CUERPO}{COMD} | {COMEN}{CUERPO}
ID_C = [:jletter:] [:jletterdigit:]*
%%
{ENTERO} {System.out.print("Entero");}
{COMENTARIO} {System.out.print("Comentario");}
{ID_C} {System.out.print("Identificador");}