// Let's play
public class FizzBuzz{

       /**
       * fizzbuzz implementation
       */
       public void fb(int limit){
           boolean a;
           for(int i = 1; i <= limit ; i++){
               a = true;
               if (i % 3 == 0){
                  System.out.print("Fizz");
                  a = false;
               }
               if(i % 5 == 0){
                  System.out.print("Buzz");
                  a = false;
               }
               if(a){
                  System.out.print(i);
               }
               System.out.print("\n");
           }
           System.out.println(2.72);
       }

       // Do the job
       public static void main (String[] args){
              FizzBuzz fb1 = new FizzBuzz();
              fb1.fb(20);
       }
}
