/* @author Javier Perez*/
package lexico;
// CODIGO JAVA; En esta sección va código fuera de la clase (import, package package)...)
%%
%{

// CODIGO JAVA; funciones custom.

/* OO style */
public int espacios;
public void muestra_espacios(){
       System.out.println("IDENTACION( "+this.espacios+" )");
}

/* Static style */


%}
%class Alexico
%public
%standalone
%unicode
ENTERO  = [1-9][0-9]* | 0+
PUNTO   = \.
IDENTIFICADOR = [:jletter:] [:jletterdigit:]*
CADENASIM = \"
SALTO = \n
CUERPO = [^\\\n]
STRING = {CADENASIM}{CUERPO}*{CADENASIM}
BOOLEAN = "TRUE" | "FALSE"
RESERVADAS = "if" | "while" | "for" | "else" | "elif" | "int" | "and" | "or" | "not" | "print" | "in" | "range" | "(" | ")" | ","
OPERADOR = "+" | "-" | "*" | "/" | "//" | "%" | "<" | ">" | "<=" | ">=" | "=" | "!" | "=="
SEPARADOR = ":"
%%
{ENTERO}      { System.out.print("ENTERO("+yytext() + ")"); }
{BOOLEAN}      { System.out.print("BOOLEAN0("+yytext() + ")"); }
{SEPARADOR}	{ System.out.print("SEPARADOR("+yytext() + ")"); }
{SALTO}	{ System.out.print("SALTO\n"); this.muestra_espacios(); this.espacios = 0; }
{ENTERO}? {PUNTO} {ENTERO} | {ENTERO} {PUNTO} {ENTERO}?
	      {System.out.print("REAL(" + yytext() + ")");} 
{STRING}	{ System.out.print("STRING(" + yytext() + ")"); }	    	      
{RESERVADAS}	{System.out.print("PALABRA RESERVADA("+yytext() + ")");}
{OPERADOR}	{System.out.print("OPERADOR("+yytext() + ")");}
{IDENTIFICADOR}     { System.out.print("IDENTIFICADOR("+yytext() + ")"); }
" "           { this.espacios++; }
//  this.muestra_espacios(); this.espacios = 0;
.             { System.out.println("Error léxico"); System.exit(0);}